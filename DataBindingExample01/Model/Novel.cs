﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataBindingExample01.Model
{
    class Novel
    {
        public string Author { get; set; }
        public string Title { get; set; }
        public int Rating { get; set; }
    }
}
