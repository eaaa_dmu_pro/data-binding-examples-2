﻿namespace DataBindingExample04.Model
{
    class PassivePerson
    {
        public string Name { get; set; }
        public int Age { get; set; }

        public override string ToString()
        {
            return $"Person: {Name}, age {Age}!";
        }
    }
}
