﻿using System.ComponentModel;

namespace DataBindingExample04.Model
{
    public class Person : INotifyPropertyChanged
    {
        // INotifyPropertyChanged Members
        public event PropertyChangedEventHandler PropertyChanged;

        protected void Notify(string propName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propName));
        }

        string name;
        public string Name
        {
            get { return this.name; }
            set
            {
                if (this.name == value) { return; }
                this.name = value;

                // OBS!!
                Notify(nameof(Name));
            }
        }

        int age;
        public int Age
        {
            get { return this.age; }
            set
            {
                if (this.age == value) { return; }
                this.age = value;

                // OBS!!
                Notify(nameof(Age));
            }
        }

        public override string ToString()
        {
            return $"Person: {Name}, age {Age}!";
        }
    }
}
