﻿using DataBindingExample04.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DataBindingExample04
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Person person = new Person() { Name = "Tom", Age = 11 };
        //PassivPerson person = new PassivPerson() { Name = "Tom", Age = 11 };

        public MainWindow()
        {
            InitializeComponent();
            this.birthdayButton.Click += birthdayButton_Click;

            // set binding for tBoxName
            Binding binding = new Binding();
            binding.Source = person;
            binding.Path = new PropertyPath(nameof(person.Name));
            binding.Mode = BindingMode.TwoWay;
            tBoxName.SetBinding(TextBox.TextProperty, binding);

            // set binding for tBoxAge
            binding = new Binding();
            binding.Source = person;
            binding.Path = new PropertyPath(nameof(person.Age));
            binding.Mode = BindingMode.TwoWay;
            tBoxAge.SetBinding(TextBox.TextProperty, binding);
        }

        void birthdayButton_Click(object sender, RoutedEventArgs e)
        {
            person.Age++;
            MessageBox.Show($"Happy Birthday, {person.Name}, age {person.Age}!", "Birthday");
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            person.Age = Convert.ToInt32(tBoxSetAge.Text);
        }

        private void btnShowObject_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show(person.ToString());
        }

    }
}
