﻿using DataBindingExample06.Model;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace DataBindingExample06
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Car car = new Car { Model = "VW Passat", ProdYear = 2010, IsRegistered = true, RegNr = "EH20345" };
        //Car car = new Car { Model = "Tesla Model 3", ProdYear = 2018, IsRegistered = false };

        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            TopStackPanel.DataContext = car;

            Binding b = new Binding();
            //b.Source = car;
            b.Converter = new BooleanToVisibilityConverter();
            b.Mode = BindingMode.OneWay;
            b.Path = new PropertyPath(nameof(Car.IsRegistered));

            tBoxRegNr.SetBinding(TextBox.VisibilityProperty, b);
        }

        private void btnShowCar_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show(car.ToString());
        }

        private void btnPlus_Click(object sender, RoutedEventArgs e)
        {
            car.ProdYear++;
        }

        private void btnMinus_Click(object sender, RoutedEventArgs e)
        {
            car.ProdYear--;
        }
    }
}
