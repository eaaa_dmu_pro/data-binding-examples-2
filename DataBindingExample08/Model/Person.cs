﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataBindingExample08.Model
{
    class Person : INotifyPropertyChanged
    {
        // INotifyPropertyChanged Members
        public event PropertyChangedEventHandler PropertyChanged;
        protected void Notify(string propName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propName));
        }

        string name;
        public string Name
        {
            get { return this.name; }
            set
            {
                if (this.name == value) { return; }
                this.name = value;
                Notify(nameof(Name));
            }
        }

        int age;
        public int Age
        {
            get { return this.age; }
            set
            {
                if (this.age == value) { return; }
                this.age = value;
                Notify(nameof(Age));
            }
        }
    }

    class PersonSorter : IComparer
    {
        public int Compare(object x, object y)
        {
            Person lhs = (Person)x;
            Person rhs = (Person)y;

            // Sort Name ascending and Age descending
            int nameCompare = lhs.Name.CompareTo(rhs.Name);
            if (nameCompare != 0)
                return nameCompare;
            return rhs.Age - lhs.Age;
        }
    }
}
