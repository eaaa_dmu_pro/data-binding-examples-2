﻿using DataBindingExample02.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DataBindingExample02
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        List<Novel> novels = new List<Novel>()
        {
            new Novel { Author = "Robert Goddard", Title = "Caugth in the light", Rating = 9 },
            new Novel { Author = "Elsebeth Egholm", Title = "Selvrisiko", Rating = 5 },
            new Novel { Author = "Lisa Marklund", Title = "Nobels testamente", Rating = 7 },
            new Novel { Author = "Jo Nesbø", Title = "Snemanden", Rating = 9 }
        };

        public MainWindow()
        {
            InitializeComponent();
            NovelsListBox.ItemsSource = novels;
        }

        private void DisplayAuthor(object sender, RoutedEventArgs e)
        {
            NovelsListBox.DisplayMemberPath = nameof(Novel.Author);
        }

        private void DisplayTitle(object sender, RoutedEventArgs e)
        {
            NovelsListBox.DisplayMemberPath = "Title";
        }

        private void NoMemberPath(object sender, RoutedEventArgs e)
        {
            NovelsListBox.DisplayMemberPath = "";
        }

    }
}
