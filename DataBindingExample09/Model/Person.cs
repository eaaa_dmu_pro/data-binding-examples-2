﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataBindingExample09.Model
{
    class Person
    {
        public string Name { get; set; }
        public int BirthYear { get; set; }
    }
}
